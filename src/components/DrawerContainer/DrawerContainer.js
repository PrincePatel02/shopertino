import React, { useRef } from 'react';
import { View } from 'react-native';
import { useColorScheme } from 'react-native-appearance';
import DrawerItem from '../DrawerItem/DrawerItem';
import DataAPIManager from '../../apis/DataAPIManager';
import deviceStorage from '../../utils/deviceStorage';
import AppStyles from '../../AppStyles';
import dynamicStyles from './styles';
import { IMLocalized } from '../../Core/localization/IMLocalization';
import { logout } from '../../redux';

function DrawerContainer(props) {
  const colorScheme = useColorScheme();
  const styles = dynamicStyles(colorScheme);
  const { navigation, appConfig } = props;

  const dataAPIManager = useRef(new DataAPIManager(appConfig));

  const onLogout = async () => {
    await deviceStorage.logoutDeviceStorage();
    await dataAPIManager?.current?.logout();
    navigation.reset({
      index: 0,
      routes: [
        {
          name: 'LoadScreen',
          params: { appStyles: AppStyles, appConfig: appConfig },
        },
      ],
    });
  };

  return (
    <View style={styles.content}>
      <View style={styles.container}>
        <DrawerItem
          title={IMLocalized('HOME')}
          source={AppStyles.iconSet.homeDrawer}
          onPress={() => {
            navigation.navigate('Home', { appConfig });
          }}
        />
        <DrawerItem
          title={IMLocalized('SHOP')}
          title="SHOP"
          source={AppStyles.iconSet.shopDrawer}
          onPress={() => {
            navigation.navigate('Shop', { appConfig });
          }}
        />
        <DrawerItem
          title={IMLocalized('BAG')}
          title="BAG"
          source={AppStyles.iconSet.shoppingBagDrawer}
          onPress={() => {
            navigation.navigate('ShoppingBag', { appConfig });
          }}
        />
        <DrawerItem
          title={IMLocalized('SEARCH')}
          source={AppStyles.iconSet.searchDrawer}
          onPress={() => {
            navigation.navigate('Search', { appConfig });
          }}
        />
        <DrawerItem
          title={IMLocalized('ORDERS')}
          source={AppStyles.iconSet.orderDrawer}
          onPress={() => {
            navigation.navigate('Order', { appConfig });
          }}
        />
        <DrawerItem
          title={IMLocalized('WISHLIST')}
          source={AppStyles.iconSet.wishlistDrawer}
          onPress={() => {
            navigation.navigate('Wishlist', { appConfig });
          }}
        />
        <DrawerItem
          title={IMLocalized('PROFILE')}
          source={AppStyles.iconSet.profileDrawer}
          onPress={() => {
            navigation.navigate('Profile', { appConfig });
          }}
        />
        <DrawerItem
          title={IMLocalized('LOGOUT')}
          source={AppStyles.iconSet.logoutDrawer}
          onPress={onLogout}
        />
      </View>
    </View>
  );
}

export default DrawerContainer;
