//default
import axios from 'axios';
import appConfig from '../ShopertinoConfig';

//wooCommerce
export * from './wooCommerce';

export default axios.create(appConfig.stripeEnv.API);
