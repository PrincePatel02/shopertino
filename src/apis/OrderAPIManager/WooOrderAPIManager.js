import { Alert } from 'react-native';
import Loading from 'react-native-loader-overlay';
import uuid from 'uuidv4';
import stripe from 'tipsi-stripe';
import { wooCommerceDataManager } from '../wooCommerce';
import Order from '../../models/Order';
import PaymentRequestAPI from '../../Core/payment/api';
import { IMLocalized } from '../../Core/localization/IMLocalization';

export default class {
  constructor(props, appConfig, loading, shoppingBag, totalPrice) {
    this.props = props;
    this.appConfig = appConfig;
    this.loading = loading;
    this.shoppingBag = shoppingBag;
    this.totalPrice = totalPrice;
    this.paymentRequestAPI = new PaymentRequestAPI(appConfig);
  }

  handleAppStateChange() {}

  startCheckout = async (selectedPaymentMethod, items, options) => {
    if (selectedPaymentMethod.isNativePaymentMethod) {
      this.handleNativePaymentMethod(items, options);
      return;
    }

    this.handleNonNativePaymentMethod();
  };

  handleNativePaymentMethod = async (items, options) => {
    try {
      const token = await stripe.paymentRequestWithNativePay(options, items);

      if (token) {
        const source = await this.paymentRequestAPI.addNewPaymentSource(
          this.props.stripeCustomer,
          token.tokenId,
        );

        this.source = source.data.response.id;

        await this.startOrder();
        stripe.completeNativePayRequest();
      } else {
        alert('An error occured, please try again.');
      }
    } catch (error) {
      console.log('native pay error', error);
      Loading.hide(this.loading);
      alert(error);
      stripe.cancelNativePayRequest();
    }
  };

  handleNonNativePaymentMethod = async () => {
    this.source = this.props.selectedPaymentMethod.cardId;
    this.startOrder();
  };

  startOrder = async (source) => {
    if (source) {
      this.source = source;
    }
    return await this.createOrder(this.source);
  };

  createOrder = async (source) => {
    const {
      totalPrice: stateTotalPrice,
      selectedShippingMethod,
      selectedPaymentMethod,
      shoppingBag: stateShoppingBag,
      user,
    } = this.props;

    let shoppingBag;
    let totalPrice;

    if (!this.shoppingBag) {
      shoppingBag = stateShoppingBag;
    } else {
      shoppingBag = this.shoppingBag;
    }

    if (!this.totalPrice) {
      totalPrice = stateTotalPrice;
    } else {
      totalPrice = this.totalPrice;
    }

    const line_items = this.getLineItems(shoppingBag);

    const wooCommerceOrder = {
      payment_method: selectedPaymentMethod.brand,
      payment_method_title: selectedPaymentMethod.title,
      set_paid: false,
      status: 'pending',
      shipping: {
        first_name: user.shipping.first_name,
        last_name: user.shipping.last_name,
        address_1: user.shipping.address_1,
        address_2: user.shipping.address_2,
        city: user.shipping.city,
        state: user.shipping.state,
        postcode: user.shipping.postcode,
        country: 'US',
      },
      line_items,
      shipping_lines: [
        {
          method_id: selectedShippingMethod.id,
          method_title: selectedShippingMethod.label,
          total: selectedShippingMethod.amount,
        },
      ],
      customer_id: user.id,
      totalPrice: Number(totalPrice),
    };

    this.chargeOrder(wooCommerceOrder, Number(totalPrice), source);
  };

  getLineItems = (shoppingBag) => {
    const productsItems =
      shoppingBag.length > 0
        ? [...shoppingBag]
        : this.getProductsFromOrderHistory();

    return productsItems.map((product) => {
      return {
        product_id: product.id,
        quantity: product.quantity ? product.quantity : 1,
        meta_data: this.getLineItemMetaData(product),
      };
    });
  };

  getLineItemMetaData = (product) => {
    if (
      product.selectedAttributes &&
      Object.keys(product.selectedAttributes)?.length
    ) {
      return Object.values(product.selectedAttributes).map((attribute) => {
        return { key: attribute.attributeName, value: attribute.option };
      });
    }

    return [];
  };

  chargeOrder = async (order, totalPrice, source) => {
    const { selectedShippingMethod } = this.props;

    try {
      const orderCopy = {
        ...order,
        total: order.totalPrice - selectedShippingMethod.amount,
      };

      const wooOrderPending = await wooCommerceDataManager.placeOrder(
        orderCopy,
      );

      if (wooOrderPending.response.id && wooOrderPending.success) {
        const charge = await this.chargeCustomer(source, totalPrice);

        if (charge.success) {
          const wooOrderCompleted = await wooCommerceDataManager.updateOrder(
            wooOrderPending.response.id,
            {
              status: 'completed',
              set_paid: true,
            },
          );
          Loading.hide(this.loading);

          this.alertOrderPLaced(wooOrderCompleted.response);
        }

        if (!charge.success) {
          Loading.hide(this.loading);
          console.log(charge);
          alert(IMLocalized('An error occured please try again later.'));
          // alert(charge.error);
        }
      }

      if (!wooOrderPending.response.id || !wooOrderPending.success) {
        Loading.hide(this.loading);

        console.log(wooOrderPending);

        alert(wooOrderPending.error);
      }
    } catch (error) {
      console.log(error);
      Loading.hide(this.loading);
      alert(error);
    }
  };

  chargeCustomer = async (source, totalPrice) => {
    const charge = await this.paymentRequestAPI.chargeStripeCustomer({
      customer: this.props.stripeCustomer,
      amount: Number(totalPrice) * 100,
      currency: 'usd',
      source,
      uuid: uuid(),
    });

    return charge;
  };

  alertOrderPLaced = (response) => {
    setTimeout(() => {
      Alert.alert(
        IMLocalized('Congratulations!'),
        IMLocalized('Your order has been placed successfully.'),
        [
          {
            text: 'OK',
            onPress: () => this.handleOrderPlaced(response),
          },
        ],
        { cancelable: true },
      );
    }, 1000);
  };

  handleOrderPlaced = async (response) => {
    const {
      selectedShippingMethod,
      selectedPaymentMethod,
      user,
      shoppingBag: stateShoppingBag,
    } = this.props;

    let shoppingBag;

    if (!this.shoppingBag) {
      shoppingBag = stateShoppingBag;
    } else {
      shoppingBag = this.shoppingBag;
    }

    const modelledOrder = new Order(
      new Date(),
      response.id,
      response.status,
      response.total,
      shoppingBag.length > 0
        ? [...shoppingBag]
        : this.getProductsFromOrderHistory(),
      user,
      selectedShippingMethod,
      selectedPaymentMethod,
      user.shippingAddress,
      user.id,
    );

    await this.props.resetCheckout();
    Loading.hide(this.loading);
    this.props.onCancelPress && this.props.onCancelPress();
    this.props.navigation.navigate('Order', { appConfig: this.appConfig });
  };

  getProductsFromOrderHistory = () => {
    const order = this.props.orderHistory.find((product) => {
      return product.id === this.props.currentOrderId;
    });

    if (
      order &&
      order.shopertino_products &&
      order.shopertino_products.length > 0
    ) {
      return order.shopertino_products;
    }
    return [];
  };
}
