import { wooCommerceManager } from '../../Core/ecommerceServices';
import PaymentRequestAPI from '../../Core/payment/api';
import { ErrorCode } from '../../Core/onboarding/utils/ErrorCode';

const defaultProfilePhotoURL =
  'https://www.iosapptemplates.com/wp-content/uploads/2019/06/empty-avatar.jpg';

const loginWithEmailAndPassword = (email, password, appConfig) => {
  return new Promise(async (resolve, reject) => {
    const wooResponse = await wooCommerceManager
      .configWooCommerceAuth(appConfig.wooCommerceConfig)
      .wooCommerceAuthManager.authCustomer({
        username: email,
        password: password,
      });

    if (wooResponse?.success && wooResponse?.response?.token) {
      const res = await getUser(email, appConfig);

      if (res.success && res.user) {
        if (!res.stripeCustomer) {
          const stripeCustomer = await createStripeCustomer(
            res.user.email,
            appConfig,
          );

          if (stripeCustomer) {
            await wooCommerceManager
              .configWooCommerceData(appConfig.wooCommerceConfig)
              .wooCommerceDataManager.updateCustomer(res.user.id, {
                billing: {
                  company: 'stripe',
                  address_1: stripeCustomer,
                },
              });
            res.stripeCustomer = stripeCustomer;
          }
        }

        res.user.stripeCustomer = res.stripeCustomer;

        resolve({
          user: res.user,
        });
      } else {
        resolve({
          user: res.user,
        });
      }
    } else {
      resolve({ error: ErrorCode.noUser });
    }
  });
};

const createAccountWithEmailAndPassword = (userDetails, appConfig) => {
  return new Promise(async (resolve, reject) => {
    const { firstName, lastName, email, password } = userDetails;

    const wooCustomer = {
      email,
      first_name: firstName,
      password,
      last_name: lastName,
    };

    const res = await wooCommerceManager
      .configWooCommerceData(appConfig.wooCommerceConfig)
      .wooCommerceDataManager.createCustomer(wooCustomer);

    if (res.success && res.response.id) {
      const userResponse = await wooCommerceManager
        .configWooCommerceData(appConfig.wooCommerceConfig)
        .wooCommerceDataManager.getCustomer({ email });
      const user = userResponse.response[0];
      if (!user) {
        resolve({ error: 'badEmailFormat' });
        return;
      }
      const authUser = {
        id: user.id,
        email,
        firstName: user.first_name,
        lastName: user.last_name,
        photoURI: user.avatar_url,
        shipping: { ...user.shipping },
        billing: {
          ...user.billing,
        },
        phone: user.billing.phone,
      };

      const stripeCustomer = await createStripeCustomer(email, appConfig);
      if (stripeCustomer) {
        await wooCommerceManager
          .configWooCommerceData(appConfig.wooCommerceConfig)
          .wooCommerceDataManager.updateCustomer(authUser.id, {
            billing: {
              company: 'stripe',
              address_1: stripeCustomer,
            },
          });
      }

      authUser.stripeCustomer = stripeCustomer;

      resolve({
        user: authUser,
      });
    } else {
      resolve({ error: res.code });
    }
  });
};

const retrievePersistedAuthUser = (appConfig) => {
  return new Promise(async (resolve, reject) => {
    const authResponse = await wooCommerceManager
      .configWooCommerceAuth(appConfig.wooCommerceConfig)
      .wooCommerceAuthManager.retrievePersistedAuthUser();

    if (authResponse.success && authResponse?.response?.token) {
      const res = await getUser(authResponse.response.user_email, appConfig);

      if (res.success && res.user) {
        res.user.stripeCustomer = res.stripeCustomer;
        resolve({
          user: res.user,
        });
      } else {
        resolve(null);
      }
    } else {
      resolve({
        error: ErrorCode.noUser,
      });
    }
  });
};

const logout = (user) => {};

const getUser = async (email, appConfig) => {
  const userResponse = await wooCommerceManager
    .configWooCommerceData(appConfig.wooCommerceConfig)
    .wooCommerceDataManager.getCustomer({ email });

  if (userResponse.response.length > 0) {
    const user = userResponse.response[0];
    const authUser = {
      id: user.id,
      email,
      firstName: user.first_name,
      lastName: user.last_name,
      photoURI: user.avatar_url,
      shipping: { ...user.shipping },
      billing: { ...user.billing },
      phone: user.billing.phone,
    };

    return {
      success: true,
      user: authUser,
      stripeCustomer: user.billing.address_1,
    };
  }

  return {
    success: false,
  };
};

const createStripeCustomer = async (email, appConfig) => {
  const paymentRequestAPI = new PaymentRequestAPI(appConfig);

  const stripeCustomer = await paymentRequestAPI.createStripeCustomer(email);

  if (stripeCustomer.success) {
    return stripeCustomer.data.customer.id;
  }

  return false;
};

const authManager = {
  retrievePersistedAuthUser,
  loginWithEmailAndPassword,
  logout,
  createAccountWithEmailAndPassword,
};

export default authManager;
