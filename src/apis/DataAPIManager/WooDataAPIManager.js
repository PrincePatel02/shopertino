import { Alert } from 'react-native';
import { wooCommerceDataManager } from '../wooCommerce';
import deviceStorage from '../../utils/deviceStorage';
import { IMLocalized } from '../../Core/localization/IMLocalization';

export default class {
  unsubscribe() {}
  async loadShopData(callback) {
    let products = [];
    let categories = [];

    const {
      response,
      success,
    } = await wooCommerceDataManager.fetchMoreProducts();

    const {
      response: catResponse,
      success: catSuccess,
    } = await wooCommerceDataManager.fetchCategories();

    if (success) {
      products = response;
    }

    if (catSuccess) {
      categories = catResponse;
    }

    callback && callback({ products, categories });
  }

  async setWishlistState(props) {
    const wishlist = await deviceStorage.getWishlist(props.user.email);
    if (wishlist) {
      wishlist.map((wishlist) => {
        props.setWishlist(wishlist);
      });
    }
  }

  setWishlist(user, wishlist) {
    deviceStorage.storeWishlist(user.email, wishlist);
  }

  async loadPaymentMethod(user, callback) {
    const paymentMethods = await deviceStorage.getPaymentMethod(user.email);

    if (paymentMethods) {
      callback && callback({ paymentMethods });
    }
  }

  onUpdatePaymentMethod(props, token, source, paymentMethods) {
    if (source.success && source.data.response) {
      const newPaymentMethod = {
        ownerId: props.user.id,
        card: token.card,
      };
      paymentMethods.push(newPaymentMethod);
      props.updatePaymentMethods(paymentMethods);

      deviceStorage.storePaymentMethod(props.user.email, paymentMethods);
    }
  }

  onRemoveFromPaymentMethods(
    method,
    user,
    paymentMethods,
    removePaymentMethod,
  ) {
    paymentMethods = paymentMethods.filter((existingMethod) => {
      return existingMethod.card.cardId !== method.cardId;
    });

    if (paymentMethods) {
      deviceStorage.storePaymentMethod(user.email, paymentMethods);
      removePaymentMethod && removePaymentMethod(method);
    }
  }

  storeUserShippAddress(props, address) {
    const data = {
      shipping: {
        first_name: address.name,
        last_name: address.name,
        address_1: address.apt,
        address_2: address.address,
        city: address.city,
        state: address.state,
        postcode: address.zipCode,
        country: 'US',
      },
    };

    props.setUserData({
      user: {
        ...props.user,
        shipping: data.shipping,
        stripeCustomer: props.user.billing.address_1,
      },
    });

    wooCommerceDataManager.updateCustomer(props.user.id, data);
  }

  onUpdateUser(props, userData) {
    const { firstName, lastName, phone } = userData;
    const data = {
      first_name: firstName,
      last_name: lastName,
      billing: {
        first_name: firstName,
        last_name: lastName,
        phone: phone,
      },
      shipping: {
        first_name: firstName,
        last_name: lastName,
      },
    };
    const user = { ...props.user, ...userData };

    wooCommerceDataManager.updateCustomer(props.user.id, data);

    props.setUserData({
      user: { ...user, stripeCustomer: props.stripeCustomer },
    });

    props.navigation.goBack();
  }

  async loadOrder(user, callback) {
    const params = {
      customer: user.id,
    };
    const { response, success } = await wooCommerceDataManager.fetchOrders(
      user,
      params,
    );

    if (response && success) {
      callback && callback(response);
      return;
    }
    callback && callback();
  }

  onShoppingBagContinuePress(props, appConfig, callback) {
    if (props.shoppingBag.length < 1) {
      return;
    }
    props.setSubtotalPrice(Number(props.totalShoppinBagPrice));

    if (!props.stripeCustomer) {
      Alert.alert(
        IMLocalized('Oops! We are unable to continue this order.'),
        IMLocalized(
          'An unknown error occured and ur account will be logged out. Afterwards, Kindly login and try again.',
        ),
        [
          {
            text: 'Ok',
            onPress: () => callback && callback(),
          },
        ],
        { cancelable: true },
      );

      return;
    }

    props.navigation.navigate('PaymentMethod', {
      appConfig,
    });
  }

  logout() {}
}
